## Taiko Web Plugins
Expand the feature set of [Taiko Web](https://github.com/bui/taiko-web) with plugins. Save these .taikoweb.js files into a folder (right click and save as on the links below) and load them in-game via the Plugins menu.

### Gameplay
- [**Change Music Speed**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/change-music-speed.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/change-music-speed.taikoweb.js?inline=false)  
  Slow down or speed up the music in game
  - (ja) [音楽の速度を変更](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/change-music-speed.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/change-music-speed.taikoweb.js?inline=false)  
    音楽の速度を変更します

- [**Change Timing Window**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/change-timing-window.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/change-timing-window.taikoweb.js?inline=false)  
  Custom input interval for in-game notes

- [**Custom Scroll Speed**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/custom-scroll-speed.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/custom-scroll-speed.taikoweb.js?inline=false)  
  Changes the speed the notes scroll at in game

- [**Rainbow Crown**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/rainbow-crown.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/rainbow-crown.taikoweb.js?inline=false)  
  Donder-Full Combos will give rainbow crowns

- [**Skip Results in Multiplayer**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/skip-results-in-multiplayer.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/skip-results-in-multiplayer.taikoweb.js?inline=false)  
  Enables skipping the results screen in multiplayer, however, the other player will not get to see the full results screen without the plugin
  - [Userscript version](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/userscripts/skip-results-in-multiplayer.user.js)

- [**Spartan Mode**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/spartan-mode.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/spartan-mode.taikoweb.js?inline=false)  
  End song on first bad hit

- [**Multiplayer Custom Songs**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/multiplayer-custom-songs.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/gameplay/multiplayer-custom-songs.taikoweb.js?inline=false)  
  Extends netplay and session multiplayer to custom song lists, both players are required to have the same folders

### Song Select
- [**Change Song Select Speed**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/song-select/change-song-select-speed.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/song-select/change-song-select-speed.taikoweb.js?inline=false)  
  Changes the song selection scroll speed

- [**Download Chart**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/song-select/download-chart.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/song-select/download-chart.taikoweb.js?inline=false)  
  Enables downloading chart and music files from the song select

### Workarounds
- [**Offline Account**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/workarounds/offline-account.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/workarounds/offline-account.taikoweb.js?inline=false)  
  Allows setting your name and customizing your Don without logging in

- [**Old Song List**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/workarounds/old-song-list.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/workarounds/old-song-list.taikoweb.js?inline=false)  
  Restores the default taiko.bui.pm song list to show non-custom songs

- [**Plugins in Old Taiko Web (Userscript)**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/userscripts/plugins-in-old-taiko-web.user.js)  
  Implements the plugin interface in older versions of taiko-web, a userscript manager extension is required

### Additional Languages
- [**Language: Spanish (Latin America)**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/languages/language-es-419.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/languages/language-es-419.taikoweb.js?inline=false)
  - (es-419) [Idioma: Español (Latinoamérica)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/languages/language-es-419.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/languages/language-es-419.taikoweb.js?inline=false)

- [**Language: French**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/languages/language-fr.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/languages/language-fr.taikoweb.js?inline=false)
  - (fr) [Langue: Français](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/languages/language-fr.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/languages/language-fr.taikoweb.js?inline=false)

- [**Language: Polish**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/languages/language-pl.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/languages/language-pl.taikoweb.js?inline=false)
  - (pl) [Język: Polski](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/languages/language-pl.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/languages/language-pl.taikoweb.js?inline=false)

### Accessibility
- [**Disable Animations**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/accessibility/disable-animations.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/accessibility/disable-animations.taikoweb.js?inline=false)  
  Turn off most of the animated elements in the game

- [**D-pad Axis Input**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/accessibility/dpad-axis-input.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/accessibility/dpad-axis-input.taikoweb.js?inline=false)  
  Binds axis input to D-pad for gamepads

- [**Backup and Restore Scores**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/accessibility/backup-and-restore-scores.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/accessibility/backup-and-restore-scores.taikoweb.js?inline=false)  
  Save and load score data to a file

### Custom Songs
- [**Green Notes**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/custom-songs/green-notes.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/custom-songs/green-notes.taikoweb.js?inline=false)  
  Adds support for green notes (G) and ad-lib notes (F) in custom charts

- [**Custom Barlines**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/custom-songs/custom-barlines.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/custom-songs/custom-barlines.taikoweb.js?inline=false)  
  Adds #BARLINESCROLL and #BARLINE to the tja format. #BARLINESCROLL can be used to set independent speed values on the measure lines, can be set to a floating point value or off. #BARLINE can insert measure lines anywhere in the chart.

- [**Loading Background**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/custom-songs/loading-background.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/custom-songs/loading-background.taikoweb.js?inline=false)  
  Shows a custom loading background if a loading.png file is in the same directory as the chart

- [**Fumen File Format**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/custom-songs/fumen-file-format.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/custom-songs/fumen-file-format.taikoweb.js?inline=false)  
  Adds support for using Fumen files in the custom song list

- [**Donkey Konga Mode**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/custom-songs/donkey-konga-mode.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/custom-songs/donkey-konga-mode.taikoweb.js?inline=false)  
  Adds support for custom Donkey Konga charts (GAME:Bongo)

### For Fun
- [**Big OK**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/for-fun/big-ok.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/for-fun/big-ok.taikoweb.js?inline=false)  
  When you get an OK judge score, the whole screen becomes a big OK

### Debugging and Testing
- [**Millisecond Accuracy**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/millisecond-accuracy.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/millisecond-accuracy.taikoweb.js?inline=false)  
  Replaces the judge score with the accuracy in milliseconds
  - (ja) [ミリ秒単位の精度](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/millisecond-accuracy.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/millisecond-accuracy.taikoweb.js?inline=false)  
    判定点数をミリ秒単位の精度に置き換えます

- [**Show BPM**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/show-bpm.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/show-bpm.taikoweb.js?inline=false)  
  Displays the current BPM in game
  - (ja) [BPMを表示する](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/show-bpm.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/show-bpm.taikoweb.js?inline=false)  
    ゲーム中のBPMを表示します

- [**Display Game Cache**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/display-game-cache.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/display-game-cache.taikoweb.js?inline=false)  
  Appends cached assets below the game

- [**Debug Button**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/debug-button.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/debug-button.taikoweb.js?inline=false)  
  Adds a button to access the debug window

- [**Convert Lyrics to vtt**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/convert-lyrics-to-vtt.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/debugging-testing/convert-lyrics-to-vtt.taikoweb.js?inline=false)  
  Adds an option to the pause screen to download converted lyrics in WEBVTT format

### Examples for Developers
- [**Language: English**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/examples/language-en.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/examples/language-en.taikoweb.js?inline=false)  
  Example language plugin for translators

- [**Example Plugin**](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/examples/example-plugin.taikoweb.js) [(⬇️)](https://gitlab.com/KatieFrogs/taiko-web-plugins/-/raw/main-gitlab/examples/example-plugin.taikoweb.js?inline=false)  
  Replaces the judge score with great/cool/miss
